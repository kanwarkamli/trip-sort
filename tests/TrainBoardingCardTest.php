<?php

namespace TripSort\Test;

use TripSort\BoardingCard\TrainBoardingCard;

class TrainBoardingCardTest extends \PHPUnit_Framework_TestCase
{
    public function testTrainBoardingCardReturnCorrectString()
    {
        $trainBoardingCard = new TrainBoardingCard('Manchester', 'Liverpool', '12K', '55M');

        $this->assertEquals($trainBoardingCard->toString(), 'Take train 55M from Manchester to Liverpool. Sit in seat 12K.');
    }
}