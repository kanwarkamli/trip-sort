<?php

namespace TripSort\Test;

use TripSort\BoardingCard\AirportBusBoardingCard;

class AirportBusBoardingCardTest extends \PHPUnit_Framework_TestCase
{
    public function testAirportBusBoardingCardReturnCorrectString()
    {
        $airportBusBoardingCard = new AirportBusBoardingCard('Liverpool', 'Bilbao Airport');

        $this->assertEquals($airportBusBoardingCard->toString(), 'Take the airport bus from Liverpool to Bilbao Airport. No seat assignment.');
    }
}