<?php

namespace TripSort\Test;

use TripSort\BoardingCard\AirportBusBoardingCard;
use TripSort\BoardingCard\FlightBoardingCard;
use TripSort\BoardingCard\TrainBoardingCard;
use TripSort\Trip;

class TripTest extends \PHPUnit_Framework_TestCase
{
    public function testTripSort()
    {
        $trip = new Trip();

        $trip->addCard(new AirportBusBoardingCard('Liverpool', 'Bilbao Airport'));
        $trip->addCard(new TrainBoardingCard('Manchester', 'Liverpool', '12K', '55M'));
        $trip->addCard(new FlightBoardingCard('Singapore', 'KLIA', '29H', 'MH90', '27'));
        $trip->addCard(new FlightBoardingCard('Bilbao Airport', 'Singapore', '13D', 'SG009', '20C', '11'));

        $trip->sortCard();

        $output = "<ol>" .
                     "<li>Take train 55M from Manchester to Liverpool. Sit in seat 12K.</li>" .
                     "<li>Take the airport bus from Liverpool to Bilbao Airport. No seat assignment.</li>" .
                     "<li>From Bilbao Airport, take flight SG009 to Singapore. Gate 20C, seat 13D. Baggage drop at ticket counter 11.</li>" .
                     "<li>From Singapore, take flight MH90 to KLIA. Gate 27, seat 29H. Baggage will be automatically transferred from your last leg.</li>" .
                     "<li>You have arrived at your final destination.</li>" .
                  "</ol>";

        $this->assertEquals($trip->toHtml(), $output);
    }
}