<?php

namespace TripSort\Test;

use TripSort\BoardingCard\FlightBoardingCard;

class FlightBoardingCardTest extends \PHPUnit_Framework_TestCase
{
    public function testFlightBoardingCardReturnCorrectString()
    {
        $flightBoardingCard = new FlightBoardingCard('Bilbao Airport', 'Singapore', '13D', 'SG009', '20C', '11');

        $this->assertEquals($flightBoardingCard->toString(), 'From Bilbao Airport, take flight SG009 to Singapore. Gate 20C, seat 13D. Baggage drop at ticket counter 11.');
    }
}