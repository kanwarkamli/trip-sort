<?php

namespace TripSort\BoardingCard;

use TripSort\BoardingCard\Common\AbstractBoardingCard;

class FlightBoardingCard extends AbstractBoardingCard
{
    private $flight, $gate, $counter;

    public function __construct($origin, $destination, $seat, $flight, $gate, $counter = null)
    {
        parent::__construct($origin, $destination, $seat);

        $this->flight = $flight;
        $this->gate = $gate;
        $this->counter = $counter;
    }

    public function toString()
    {
        return 'From ' .
            $this->get('origin') . ', take flight ' .
            $this->flight . ' to ' . $this->get('destination') .
            '. Gate ' . $this->gate . ', seat ' . $this->get('seat') . '. ' .
            ($this->counter ? 'Baggage drop at ticket counter ' . $this->counter . '.' : 'Baggage will be automatically transferred from your last leg.');
    }
}