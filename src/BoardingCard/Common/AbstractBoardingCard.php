<?php

namespace TripSort\BoardingCard\Common;


abstract class AbstractBoardingCard
{
    protected $origin, $destination, $seat;

    function __construct($origin, $destination, $seat)
    {
        $this->origin = $origin;
        $this->destination = $destination;
        $this->seat = $seat;
    }

    public function set($name, $value)
    {
        $this->$name = $value;
    }

    public function get($name)
    {
        return $this->$name;
    }
}