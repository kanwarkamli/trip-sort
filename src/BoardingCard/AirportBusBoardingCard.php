<?php

namespace TripSort\BoardingCard;

use TripSort\BoardingCard\Common\AbstractBoardingCard;

class AirportBusBoardingCard extends AbstractBoardingCard
{
    public function __construct($origin, $destination, $seat = null)
    {
        parent::__construct($origin, $destination, $seat);
    }

    public function toString()
    {
        return 'Take the airport bus from ' .
            $this->get('origin') . ' to ' .
            $this->get('destination') . '. ' .
            ($this->get('seat') ? 'Sit in seat ' . $this->get('seat') . '.' : 'No seat assignment.');
    }
}