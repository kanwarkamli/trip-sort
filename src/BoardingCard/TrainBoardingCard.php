<?php

namespace TripSort\BoardingCard;

use TripSort\BoardingCard\Common\AbstractBoardingCard;

class TrainBoardingCard extends AbstractBoardingCard
{
    private $train;

    public function __construct($origin, $destination, $seat, $train)
    {
        parent::__construct($origin, $destination, $seat);

        $this->train = $train;
    }

    public function toString()
    {
        return 'Take train ' .
            $this->train . ' from ' .
            $this->get('origin') . ' to ' .
            $this->get('destination') .
            '. Sit in seat ' . $this->get('seat') . '.';
    }
}