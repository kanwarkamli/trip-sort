<?php

namespace TripSort\Utils\Interfaces;

interface SortInterface {
    public static function sort(array $items);
}
