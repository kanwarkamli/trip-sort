<?php

namespace TripSort\Utils\Sorters;

use TripSort\Utils\Interfaces\SortInterface;

class ArraySorter implements SortInterface
{
    public static function sort(array $items)
    {
        /*
         * This indexing step takes O(n) time.
         *
         *  Index the departure and arrival locations for fast lookup.
        */
        $departureIndex = self::createDepartureIndex($items);
        $arrivalIndex = self::createArrivalIndex($items);

        /*
         * This next step also takes O(n) time.
         */
        $startingLocation = self::getStartingLocation($items, $arrivalIndex);

        /*
         *  From the starting location, traverse the boarding passes, creating a sorted list as we go.
         *
         * This step takes O(n) time.
         */

        $sortedBaseBoardingCards = array();
        $currentLocation = $startingLocation;
        /*
         * Assign respective boarding pass while checking for undefined index
         */
        while ($currentBaseBoardingCard = (array_key_exists($currentLocation, $departureIndex)) ? $departureIndex[$currentLocation] : null) {
            /*
             *  Add the boarding pass to our sorted list.
             */
            array_push($sortedBaseBoardingCards, $currentBaseBoardingCard);

            /*
             *  Get our next location.
             */
            $currentLocation = $currentBaseBoardingCard->get('destination');
        }

        /*
         * After O(3n) operations, we can now return the sorted boarding passes.
         */

        return $sortedBaseBoardingCards;
    }

    public static function createDepartureIndex($boardingCards)
    {
        foreach ($boardingCards as $baseBoardingCard) {
            $departureIndex[$baseBoardingCard->get('origin')] = $baseBoardingCard;
        }

        return $departureIndex;
    }

    public static function createArrivalIndex($boardingCards)
    {
        foreach ($boardingCards as $baseBoardingCard) {
            $arrivalIndex[$baseBoardingCard->get('destination')] = $baseBoardingCard;
        }

        return $arrivalIndex;
    }

    private static function getStartingLocation($baseBoardingCards, $arrivalIndex)
    {
        foreach($baseBoardingCards as $baseBoardingCard) {
            $departureLocation = $baseBoardingCard->get('origin');

            if (!array_key_exists($departureLocation, $arrivalIndex)) {
                return $departureLocation;
            }
        }

        return null;
    }
}
