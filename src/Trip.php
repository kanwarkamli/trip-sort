<?php

namespace TripSort;

use TripSort\BoardingCard\Common\AbstractBoardingCard;
use TripSort\Utils\Sorters\ArraySorter;

class Trip
{
    const FINAL_DESTINATION = 'You have arrived at your final destination.';

    private $boardingCard, $sortedBaseBoardingCard;

    public function getBoardingCards()
    {
        return $this->boardingCard;
    }

    public function sortCard()
    {
        $this->sortedBaseBoardingCard = ArraySorter::sort($this->boardingCard);
    }

    public function addCard(AbstractBoardingCard $boardingCard)
    {
        $this->boardingCard[] = $boardingCard;
    }

    public function toHtml()
    {
        $str = '<ol>';

        foreach( $this->sortedBaseBoardingCard as $boarding) {
            $str .= '<li>' . $boarding->toString() . '</li>' ;
        }

        $str .= '<li>' . self::FINAL_DESTINATION . '</li>';
        $str .= '</ol>';

        return $str;
    }

    public function toString()
    {
        $str = '';

        foreach( $this->sortedBaseBoardingCard as $boarding) {
            $str .= $boarding->toString() . PHP_EOL ;
        }

        $str .= self::FINAL_DESTINATION . PHP_EOL;

        echo $str;
    }
}