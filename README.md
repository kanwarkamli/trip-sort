### How to execute the code
```
php index.php
```


## Installation
Using Composer :

```
git clone git@bitbucket.org:kanwarkamli/trip-sort.git
cd trip-sort
composer install
```


### Extending class
* New type of card can be added by extending the AbstractBoardingCard.