<?php

namespace TripSort;

require_once __DIR__ . '/vendor/autoload.php';

use TripSort\BoardingCard\AirportBusBoardingCard;
use TripSort\BoardingCard\FlightBoardingCard;
use TripSort\BoardingCard\TrainBoardingCard;

// Application starts
$trip = new Trip();

$trip->addCard(new TrainBoardingCard('Madrid', 'Barcelona', '45B', '78A'));
$trip->addCard(new FlightBoardingCard('Stockholm', 'New York JFK', '7B', 'SK22', '22'));
$trip->addCard(new FlightBoardingCard('Gerona Airport', 'Stockholm', '3A', 'SK455', '45B', '344'));
$trip->addCard(new AirportBusBoardingCard('Barcelona', 'Gerona Airport'));
//$trip->addCard(new FlightBoardingCard('New York JFK', 'KLIA', '69B', 'MH13', '9', '6'));

$trip->sortCard();

//echo ($trip->toHtml());

$trip->toString();